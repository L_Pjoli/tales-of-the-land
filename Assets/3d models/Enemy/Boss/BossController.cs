﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BossController : MonoBehaviour
{
    // Start is called before the first frame update
    public NavMeshAgent agent;
    private GameObject target;
    public Animator animator;
    public GameObject[] projectileType;
    public float projectileSpeed = 100f;
    public float fireRange;
    public GameObject projectileEmitter;

    public float fireRate = 10;

    private float timeStamp;
    private bool startUp;
    private bool isDead;
    
    void Awake()
    {
        target = GameObject.Find("Player");
    }
    void Start()
    {
        fireRange = agent.stoppingDistance;
        
        timeStamp = Time.time + fireRate;

        startUp = false;
        isDead = false;
        animator.SetBool("IsDead",false);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 targetPos = target.transform.position;
        NavMeshPath path = new NavMeshPath();

        agent.CalculatePath(targetPos, path);


        if(isDead){
            animator.SetBool("IsDead",true);
            agent.isStopped = true;
            if(animator.GetCurrentAnimatorStateInfo(0).IsName("Death")){
                Destroy(gameObject);
            }
        }

        if(path.status == NavMeshPathStatus.PathComplete){
            agent.SetDestination(targetPos);
            agent.isStopped = true;

            if(startUp){
                if( !animator.GetCurrentAnimatorStateInfo(0).IsName("Roar") && !AttackAnimationDone() ){
                    animator.SetBool("IsChasing",true);
                    animator.SetBool("IsAttacking",false);

                    bool isInFiringRange = agent.remainingDistance < fireRange;

                    if( isInFiringRange ){
                        animator.SetBool("IsChasing",false);
                        
                        Firing();

                    }else{
                        animator.SetBool("IsChasing",true);
                        agent.isStopped = false;
                        if( AttackAnimationDone() ){
                            agent.isStopped = true;
                        }
                    }
                } 
            }
            else
            {
                animator.SetBool("IsChasing",false);
            }    
        }
        
        if(agent.remainingDistance < fireRange){
            transform.LookAt(targetPos);
        }

        startUp = true;
    }

    private void FireProjectile()
    {  
        int randomProjectileType = Random.Range(0,4);
        GameObject projectile;

        switch(randomProjectileType){
            case 0:
                animator.SetInteger("AttackType",0);
            break;

            case 1:
                animator.SetInteger("AttackType",1);
            break;
                
            case 2:
                animator.SetInteger("AttackType",2);
            break;

            case 3:
                animator.SetInteger("AttackType",3);
            break;
        }

        projectile = projectileType[randomProjectileType];

        GameObject instProjectile =  Instantiate(projectile, projectileEmitter.transform.position, Quaternion.identity);
        Rigidbody rb = instProjectile.GetComponent<Rigidbody>();
        rb.AddForce(transform.forward * projectileSpeed);
        timeStamp = Time.time + fireRate;

        timeStamp = Time.time + fireRate;
    }

    private void Firing()
    {
        bool firingCooldown = timeStamp <= Time.time;

        if(firingCooldown){
            animator.SetBool("IsAttacking",true);
            
            FireProjectile(); 
        }
    }

    private bool AttackAnimationDone()
    {
        return (
            animator.GetCurrentAnimatorStateInfo(0).IsName("Attack01") &&
            animator.GetCurrentAnimatorStateInfo(0).IsName("Attack02") &&
            animator.GetCurrentAnimatorStateInfo(0).IsName("Attack03") &&
            animator.GetCurrentAnimatorStateInfo(0).IsName("Attack04") );
    }
}
