﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footsteps : MonoBehaviour
{
    private CharacterController characterController = null;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        AudioSource audio = GetComponent<AudioSource>();
        if (characterController.velocity.magnitude > 1.5f && !audio.isPlaying)
        {
            audio.volume = Random.Range(0.1f, 0.2f);
            audio.pitch = Random.Range(0.8f, 1.1f);
            audio.Play();
        }
    }
}
