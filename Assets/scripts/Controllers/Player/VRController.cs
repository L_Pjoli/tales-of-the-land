﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class VRController : MonoBehaviour
{
    public float gravity = 1.0f;
    public float sensitivity = 0.1f;
    public float maxSpeed = 0.1f;
    public float rotateIncrement = 90;
    public float runMultiplicator = 1.8f;

    public SteamVR_Action_Boolean snapTurnLeft = null;
    public SteamVR_Action_Boolean snapTurnRight = null;
    public SteamVR_Action_Boolean movePress = null;
    public SteamVR_Action_Vector2 moveValue = null;
    public SteamVR_Action_Boolean run = null;

    private float speedY = 0.0f;
    private float speedX = 0.0f;


    private CharacterController characterController = null;
    public Transform cameraRig = null;
    public Transform head = null;


    private void Awake()
    {
        characterController = GetComponent<CharacterController>();
    }

    void Update()
    {
        HandleHeight();
        CalculateMovement();
        SnapRotation();
    }

    private void HandleHeight()
    {
        float headHeight = Mathf.Clamp(head.localPosition.y, 1, 2);
        characterController.height = headHeight;

        Vector3 newCenter = Vector3.zero;
        newCenter.y = characterController.height / 2;
        newCenter.y += characterController.skinWidth;

        newCenter.x = head.localPosition.x;
        newCenter.z = head.localPosition.z;

        newCenter = Quaternion.Euler(0, -transform.eulerAngles.y, 0) * newCenter;

        characterController.center = newCenter;
    }

    private void CalculateMovement()
    {
        Vector3 orientationEuler = new Vector3(0, head.eulerAngles.y, 0);
        Quaternion orientation = Quaternion.Euler(orientationEuler);
        Vector3 movement = Vector3.zero;

        speedX = 0.0f;
        speedY = 0.0f;

        
        speedY += moveValue.axis.y * sensitivity;
        speedY = Mathf.Clamp(speedY, -maxSpeed, maxSpeed);
        speedX += moveValue.axis.x * sensitivity;
        speedX = Mathf.Clamp(speedX, -maxSpeed, maxSpeed);

        if (run.GetStateDown(SteamVR_Input_Sources.Any)){
            speedX = speedX * runMultiplicator;
            speedY = speedY * runMultiplicator;
        }

        movement = orientation * ((speedY * Vector3.forward) + (speedX * Vector3.right));
        movement.y -= gravity * Time.deltaTime;

        characterController.Move(movement);
    }

    private void SnapRotation()
    {
        float snapValue = 0.0f;
        if (snapTurnLeft.GetStateDown(SteamVR_Input_Sources.Any))
        {
            snapValue = -Mathf.Abs(rotateIncrement);
        }
        if (snapTurnRight.GetStateDown(SteamVR_Input_Sources.Any))
        {
            snapValue = Mathf.Abs(rotateIncrement);
        }
        transform.RotateAround(head.position, Vector3.up, snapValue);
    }
}
