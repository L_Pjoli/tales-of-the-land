﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public float health = 10;

    void Update()
    {
        if(health <= 0)
        {
            HandleDeath();
        }
    }

    public virtual void HandleDeath()
    {
        Destroy(gameObject);
    }

    public void DoDamage(float damageAmount)
    {
        health -= damageAmount;
    }
}
