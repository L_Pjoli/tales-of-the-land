﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerTest : MonoBehaviour
{

    public float mouseSensitivity;
    public float moveSpeed;


    // Update is called once per frame
    void Update()
    {
        float moveInputHorizontal = Input.GetAxis("Horizontal");
        float moveInputVertical = Input.GetAxis("Vertical");

        float mouseInputHorizontal = Input.GetAxis("Mouse X");
        float mouseInputVertical = Input.GetAxis("Mouse Y");

        float positionY = transform.position.y;
        transform.Translate(Vector3.right * moveInputHorizontal * moveSpeed);
        transform.Translate(Vector3.forward * moveInputVertical * moveSpeed);
        transform.position = new Vector3(transform.position.x, positionY, transform.position.z);

        Vector3 rotationJoueur = transform.rotation.eulerAngles;
        rotationJoueur.y += mouseInputHorizontal * mouseSensitivity;
        rotationJoueur.x -= mouseInputVertical * mouseSensitivity;
        rotationJoueur.z = 0;

        transform.rotation = Quaternion.Euler(rotationJoueur);

    }
}
