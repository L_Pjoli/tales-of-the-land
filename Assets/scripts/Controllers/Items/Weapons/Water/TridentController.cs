﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TridentController : MonoBehaviour
{

    public float damageDealt;

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Trident: OnTriggerEnter with " + other.gameObject);
        Health hp = other.gameObject.GetComponent<Health>();
        if (hp != null)
        {
            hp.DoDamage(damageDealt);
            //DamageDealtCoroutine.ApplyOn(other.gameObject);
            Debug.Log("Trident dealt " + damageDealt + " to " + other.gameObject);
        }
    }
}
