﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class SwordController : MonoBehaviour
{
    public float speedMultiplier = 1.6f;

    private VelocityEstimator velocity;

    [Tooltip("The sound to play when the sword goes very fast.")]
    public AudioSource audio;

    public void Start()
    {
        velocity = GetComponent<VelocityEstimator>();
        velocity.estimateOnAwake = true;
        velocity.BeginEstimatingVelocity();
    }

    public void FixedUpdate()
    {
        if (velocity.GetVelocityEstimate().magnitude > 4.5)
        {
            audio.Play();
        }
    }

    public float ComputeDamage(Collision collision)
    {
        Vector3 swordVelocity = velocity.GetVelocityEstimate();
        Vector3 otherVelocity = collision.rigidbody.velocity;
        Vector3 relativeVelocity = otherVelocity - swordVelocity;
        return Mathf.Clamp(speedMultiplier * relativeVelocity.magnitude, 0, 20);
    }

    void OnCollisionEnter(Collision collision)
    {
        Health hitpoints = collision.gameObject.GetComponent<Health>();
        if (hitpoints != null)
        {
            float damageAmount = ComputeDamage(collision);
            Debug.Log("Sword has hit " + collision.gameObject.ToString() + " and has dealt " + damageAmount + " damage.");
            hitpoints.DoDamage(damageAmount);
            //DamageDealtCoroutine.ApplyOn(collision.gameObject);
        }
    }
}
