﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Handles the spawning and returning of the ItemPackage
//
//=============================================================================

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine.Events;
using Valve.VR.InteractionSystem;
#if UNITY_EDITOR
using UnityEditor;
#endif
//-------------------------------------------------------------------------
[RequireComponent(typeof(Interactable))]
public class SingleItemPackageSpawner : ItemPackageSpawner {
    
    //-------------------------------------------------
    protected override void SpawnAndAttachObject(Hand hand, GrabTypes grabType)
    {
        base.SpawnAndAttachObject(hand, grabType);
        Destroy(gameObject);
    }
}
