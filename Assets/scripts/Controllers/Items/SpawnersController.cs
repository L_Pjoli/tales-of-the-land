﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnersController : MonoBehaviour {

    protected static int nDestroyed = 0;
    protected static int startDestroyingAllAfter = 2;
    protected static bool isDestroyingEverything = false;

    private void OnDestroy()
    {
        Debug.Log("OnDestroy");
        SpawnersController.nDestroyed++;
        if (!SpawnersController.isDestroyingEverything && startDestroyingAllAfter <= nDestroyed)
        {
            Debug.Log("Destroy everything");
            SpawnersController.isDestroyingEverything = true;
            foreach (GameObject toDestroy in GameObject.FindGameObjectsWithTag("Spawner"))
            {
                Destroy(toDestroy);
            }

            Debug.Log("Starting game");
            GameController gameController = FindObjectOfType<GameController>();
            gameController.hasStarted = true;
            gameController.SpawnEnnemis();
            Debug.Log("Game started");
        }
    }
}