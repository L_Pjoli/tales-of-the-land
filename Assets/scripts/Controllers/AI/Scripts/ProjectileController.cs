﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ProjectileController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
       Destroy(gameObject,5.0f);
    }

    // Update is called once per frame
    void Update()
    { 
      
    }

    void OnCollisionEnter(Collision collision)
    {
        //Debug.Log(collision.gameObject.name);

        if(collision.gameObject.name == "Player" || collision.gameObject.name == "Wall")
        {
            Destroy(gameObject);
        }
    }

}
