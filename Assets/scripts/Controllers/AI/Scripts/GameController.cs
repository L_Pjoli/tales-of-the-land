﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GameController : MonoBehaviour {
    public int nbrRonde;
    public int nbrEnnemisDeBase;
    public float multiplicateurEnnemis;
    public GameObject[] ennemisPossible;
    public GameObject boss;
    public float XSpawnRange;
    public float ZSpawnRange;
    public AudioClip audioClip;

    private int numRonde;
    public bool isFinalRound = false;
    private GameObject enemyType;
    private int enemyCount = 0;
    private Vector3 referencePosition;
    private bool newRoundCoRoutineStarted = false;
    private AudioSource audioSource;

    public bool hasStarted = false;

    // Start is called before the first frame update
    void Start()
    {
        referencePosition = GameObject.Find("Player").transform.position;

        numRonde = 1;

        audioSource = gameObject.GetComponent<AudioSource>();

    }

    void FixedUpdate()
    {
        if (hasStarted)
        {
            if (!audioSource.isPlaying)
            {
                audioSource.Play(0);
            }
            GameObject[] ennemis = GameObject.FindGameObjectsWithTag("Enemy");

            if (ennemis.Length == 0)
            {
                audioSource.Stop();
                if (!newRoundCoRoutineStarted)
                {
                    StartCoroutine("StartNewRound");
                }
            }

            if (isGameWon())
            {
                Debug.Log("You Win");
            }
        }
    }

    private IEnumerator StartNewRound()
    {
        newRoundCoRoutineStarted = true;
        Debug.Log("begin start new round");
        yield return new WaitForSeconds(10);
        if (numRonde == nbrRonde && !isFinalRound)
        {
            // Boss round
            Debug.Log("Boss round");
            SpawnBoss();
            isFinalRound = true;
            audioSource.clip = audioClip;
            audioSource.Play(0);
        }
        else if (!isFinalRound)
        {
            // Normal round
            Debug.Log("Normal round (" + numRonde + ")");
            numRonde++;
            SpawnEnnemis();
            audioSource.Play(0);
        }
        Debug.Log("end start new round");
        newRoundCoRoutineStarted = false;
    }

    public bool isGameWon()
    {
        return isFinalRound && numRonde == nbrRonde && GameObject.FindGameObjectWithTag("Boss") == null;
    }

    public void SpawnEnnemis()
    {
        int nbrEnnemisMax = nbrEnnemisDeBase + (int)(numRonde * multiplicateurEnnemis);

        while (enemyCount <= nbrEnnemisMax)
        {
            int randomEnemyType = Random.Range(0, ennemisPossible.Length);

            enemyType = ennemisPossible[randomEnemyType];

            float randomXPos = Random.Range(-(XSpawnRange), XSpawnRange);
            float randomZPos = Random.Range(-(ZSpawnRange), ZSpawnRange);

            Vector3 spawnLocation = new Vector3(referencePosition.x + randomXPos + 2, referencePosition.y, referencePosition.z + randomZPos + 2);

            GameObject ennemy = Instantiate(enemyType, spawnLocation, Quaternion.identity);
            NavMeshAgent navAgent = ennemy.GetComponent<NavMeshAgent>();
            if (!navAgent.Warp(spawnLocation))
            {
                Debug.Log("Failed to place ennemy on the navmesh.");
                Destroy(ennemy);
                continue;
            }

            enemyCount++;
        }
    }

    void SpawnBoss()
    {

        float randomXPos = Random.Range(-(XSpawnRange), XSpawnRange) + 5;
        float randomZPos = Random.Range(-(ZSpawnRange), ZSpawnRange) + 5;

        Vector3 spawnLocation = new Vector3(referencePosition.x + randomXPos, referencePosition.y, referencePosition.z + randomZPos);

        Instantiate(boss, spawnLocation, Quaternion.identity);
    }

    public int getNumRonde()
    {
        return numRonde;
    }
}
