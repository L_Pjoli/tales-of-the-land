﻿using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour {
    public NavMeshAgent agent;
    public GameObject target;
    public GameObject projectile;
    public GameObject projectileEmitter;
    public Animator animator;

    public float projectileSpeed = 100f;
    public float fireRate = 2;
    public float aggroRange = 20;
    private float fireRange;

    private float timeStamp;
    private bool startUp;
    private bool isDead;

    void Start()
    {
        if (target == null)
            target = GameObject.Find("Player");

        fireRange = agent.stoppingDistance;
        timeStamp = Time.time + fireRate;
        startUp = false;
        isDead = false;
        animator.SetBool("IsDead", false);
    }

    // Update is called once per frame
    void Update()
    {
        if (isDead)
        {
            animator.SetBool("IsDead", true);
            agent.isStopped = true;
            if (animator.GetCurrentAnimatorStateInfo(0).IsName("Death"))
            {
                Destroy(gameObject);
            }
        }

        Vector3 targetPos = target.transform.position;
        NavMeshPath path = new NavMeshPath();

        agent.CalculatePath(targetPos, path);

        if (path.status == NavMeshPathStatus.PathComplete)
        {
            agent.SetDestination(targetPos);
            agent.isStopped = true;

            bool isInAggroRange = agent.remainingDistance < aggroRange;

            if (isInAggroRange && startUp)
            {
                animator.SetBool("IsInAggroRange", true);
                animator.SetBool("IsChasing", true);

                bool isInFiringRange = agent.remainingDistance < fireRange;

                if (isInFiringRange)
                {
                    animator.SetBool("IsChasing", false);
                    animator.SetBool("IsInFiringRange", true);
                    Firing();
                }
                else
                {
                    animator.SetBool("IsChasing", true);
                    animator.SetBool("IsInFiringRange", false);
                    agent.isStopped = false;
                    if (animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
                    {
                        agent.isStopped = true;
                    }
                }
            }
            else
            {
                animator.SetBool("IsInAggroRange", false);
                animator.SetBool("IsChasing", false);
            }
        }

        if (agent.remainingDistance < fireRange)
        {
            transform.LookAt(targetPos);
        }

        startUp = true;
    }

    private void FireProjectile()
    {
        GameObject instProjectile = Instantiate(projectile, projectileEmitter.transform.position, Quaternion.identity);
        Rigidbody rb = instProjectile.GetComponent<Rigidbody>();
        rb.AddForce(transform.forward * projectileSpeed);
        timeStamp = Time.time + fireRate;
        Debug.Log(rb.GetComponent<GenericProjectile>().damage);
    }

    private void Firing()
    {
        bool firingCooldown = timeStamp <= Time.time;

        if (firingCooldown)
        {
            animator.SetBool("IsFiring", true);
            FireProjectile();
            animator.SetBool("IsFiring", false);
        }
    }
}
