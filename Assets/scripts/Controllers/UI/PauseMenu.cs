﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Valve.VR;
using Valve.VR.Extras;

public class PauseMenu : MonoBehaviour {

    public static bool gameIsPaused = false;
    public GameObject player;
    private Camera playerCamera;
    private VRController playerVRController;
    public GameObject pauseMenuUI;
    public SteamVR_Action_Boolean pauseButton = null;
    public SteamVR_Action_Boolean selectUp = null;
    public SteamVR_Action_Boolean selectDown = null;
    public SteamVR_Action_Boolean interactUI = null;
    public Button[] buttons;
    private int selected;

    void Start() {
        selected = 0;
        playerVRController = player.GetComponent<VRController>();
        playerCamera = player.transform.GetChild(0).GetChild(3).GetComponent<Camera>();
    }

    

    // Update is called once per frame
    void Update()
    {
        if (pauseButton.GetStateDown(SteamVR_Input_Sources.Any)) {
            if (gameIsPaused) {
                Resume();
            } else {
                Pause();
            }
        }
        if (gameIsPaused) {
            if (selectUp.GetStateDown(SteamVR_Input_Sources.Any)) {
                SelectUp();
            } else if (selectDown.GetStateDown(SteamVR_Input_Sources.Any)) {
                SelectDown();
            } else if (interactUI.GetStateDown(SteamVR_Input_Sources.Any)) {
                Select();
            }
        }
    }

    void SelectUp() {
        selected--;
        if (selected < 0) {
            selected = buttons.Length-1;
        }
        SetButtonsColor();
    }

    void SelectDown() {
        selected++;
        if (selected >= buttons.Length) {
            selected = 0;
        }
        SetButtonsColor();
    }

    void Select() {
        buttons[selected].onClick.Invoke();
    }

    void SetButtonsColor() {
        for(int i = 0; i < buttons.Length; i++) {
            if(i == selected) {
                buttons[i].gameObject.GetComponent<Image>().color = new Color(0.6f, 0.6f, 0.6f);
            } else {
                buttons[i].gameObject.GetComponent<Image>().color = new Color(1f, 1f, 1f);
            }
        }
    }

    public void Resume() {
        pauseMenuUI.SetActive(false);
        playerVRController.enabled = true;
        Time.timeScale = 1f;
        gameIsPaused = false;
    }

    void Pause() {
        selected = 0;
        gameObject.transform.position = playerCamera.transform.position + playerCamera.transform.forward * 5.0f;
        gameObject.transform.rotation = new Quaternion(0.0f, playerCamera.transform.rotation.y, 0.0f, playerCamera.transform.rotation.w);
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        playerVRController.enabled = false;
        gameIsPaused = true;
        SetButtonsColor();
    }

    public void GoToMainMenu() {
        Destroy(GameObject.Find("spawn"));
        Time.timeScale = 1f;
        SceneManager.LoadSceneAsync("MainMenu");
    }

}
