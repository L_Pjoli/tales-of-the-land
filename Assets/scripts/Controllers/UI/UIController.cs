﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    private float baseHealth;
    public VRController joueur;
    public GameObject mainDroite;

    public Slider healthSlider;
    public TMPro.TextMeshProUGUI compteurRonde;

    public GameController gc;


    void Start()
    {
        baseHealth = joueur.GetComponent<Health>().health;

    }

    void Update()
    {
        healthSlider.value = ((joueur.GetComponent<Health>().health * 100) / baseHealth);
        compteurRonde.text = "" + gc.getNumRonde();
    }
}
