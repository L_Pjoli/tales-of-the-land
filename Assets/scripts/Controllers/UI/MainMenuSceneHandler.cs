﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Valve.VR;
using Valve.VR.Extras;

public class MainMenuSceneHandler : MonoBehaviour {

    private AudioSource audioSource;
    public AudioClip soundHover;
    public AudioClip soundClick;
    public SteamVR_Action_Boolean selectUp = null;
    public SteamVR_Action_Boolean selectDown = null;
    public SteamVR_Action_Boolean interactUI = null;
    public Button[] buttons;
    private int selected;

    void Start() {
        selected = 0;
        audioSource = gameObject.GetComponent<AudioSource>();
        SetButtonsColor();
    }

    void Update() {
        
            if (selectUp.GetStateDown(SteamVR_Input_Sources.Any)) {
                SelectUp();
            } else if (selectDown.GetStateDown(SteamVR_Input_Sources.Any)) {
                SelectDown();
            } else if (interactUI.GetStateDown(SteamVR_Input_Sources.Any)) {
                Select();
            }
        
    }

    void SelectUp() {
        selected--;
        if (selected < 0) {
            selected = buttons.Length - 1;
        }
        SetButtonsColor();
        audioSource.PlayOneShot(soundHover);
    }

    void SelectDown() {
        selected++;
        if (selected >= buttons.Length) {
            selected = 0;
        }
        SetButtonsColor();
        audioSource.PlayOneShot(soundHover);
    }

    void Select() {
        audioSource.PlayOneShot(soundClick);
        buttons[selected].onClick.Invoke();
    }

    void SetButtonsColor() {
        for (int i = 0; i < buttons.Length; i++) {
            if (i == selected) {
                buttons[i].gameObject.GetComponent<Image>().color = new Color(0.6f, 0.6f, 0.6f);
            } else {
                buttons[i].gameObject.GetComponent<Image>().color = new Color(1f, 1f, 1f);
            }
        }
    }

    public void StartGame() {
        Destroy(GameObject.Find("Player"));
        SceneManager.LoadSceneAsync("Arena");
    }

    public void QuitGame() {
        Application.Quit();
    }
}
