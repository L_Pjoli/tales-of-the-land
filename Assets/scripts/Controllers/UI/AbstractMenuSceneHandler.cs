﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR.Extras;

public abstract class AbstractMenuSceneHandler : MonoBehaviour
{
    public SteamVR_LaserPointer laserPointer;
    public AudioClip soundHover;
    public AudioClip soundClick;

    void Awake()
    {
        laserPointer.PointerIn += PointerInside;
        laserPointer.PointerOut += PointerOutside;
        laserPointer.PointerClick += PointerClick;
    }

    public abstract void PointerClick(object sender, PointerEventArgs e);

    public abstract void PointerInside(object sender, PointerEventArgs e);

    public abstract void PointerOutside(object sender, PointerEventArgs e);

    public void ChangeColor(PointerEventArgs e, Color color)
    {
        e.target.gameObject.GetComponent<Image>().color = color;
    }

    public void PlaySoundEffect(PointerEventArgs e, AudioClip sfx)
    {
        e.target.gameObject.GetComponent<AudioSource>().PlayOneShot(sfx);
    }
}
