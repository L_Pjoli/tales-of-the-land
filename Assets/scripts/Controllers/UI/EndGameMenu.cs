﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Valve.VR;

public class EndGameMenu : MonoBehaviour
{
    private bool gameOver = false;
    private Camera playerCamera;
    private VRController playerVRController;
    private GameController gameController;
    public GameObject player;
    public GameObject gameOverMenuUI;
    public SteamVR_Action_Boolean interactUI = null;
    public Button button;
    public TMPro.TextMeshProUGUI textEndGame;


    // Start is called before the first frame update
    void Start()
    {
        gameController = GameObject.Find("Game Controller").GetComponent<GameController>();
        playerVRController = player.GetComponent<VRController>();
        playerCamera = player.transform.GetChild(0).GetChild(3).GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (player.GetComponent<Health>().health <= 0f && gameOver == false){
            EndGame("GAME OVER");
        } else if (gameController.isGameWon() && gameOver == false) { //condition victoire
            EndGame("YOU WIN");
        } else if(gameOver) {
            if (interactUI.GetStateDown(SteamVR_Input_Sources.Any)) {
                Destroy(GameObject.Find("spawn"));
                Time.timeScale = 1f;
                SceneManager.LoadSceneAsync("MainMenu");
            }
        }


    }

    void EndGame(string endGameMessage) {
        gameObject.transform.position = playerCamera.transform.position + playerCamera.transform.forward * 5.0f;
        gameObject.transform.rotation = new Quaternion(0.0f, playerCamera.transform.rotation.y, 0.0f, playerCamera.transform.rotation.w);
        gameOverMenuUI.SetActive(true);
        textEndGame.text = endGameMessage;
        Time.timeScale = 0f;
        playerVRController.enabled = false;
        gameOver = true;
        GameObject.Find("PauseCanvas").GetComponent<PauseMenu>().enabled = false;
    }
}
