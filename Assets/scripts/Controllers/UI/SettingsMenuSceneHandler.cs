﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Valve.VR.Extras;

public class SettingsMenuSceneHandler : AbstractMenuSceneHandler
{
    public override void PointerClick(object sender, PointerEventArgs e)
    {
        if (e.target.name == "Return Button")
        {
            e.target.gameObject.GetComponent<Image>().color = new Color(0.6f, 0.6f, 0.6f);
            SceneManager.LoadSceneAsync("MainMenu");

        }
    }

    public override void PointerInside(object sender, PointerEventArgs e)
    {
        if (e.target.name == "Return Button")
        {
            e.target.gameObject.GetComponent<Image>().color = new Color(0.8f, 0.8f, 0.8f);
        }
    }

    public override void PointerOutside(object sender, PointerEventArgs e)
    {
        if (e.target.name == "Return Button")
        {
            e.target.gameObject.GetComponent<Image>().color = new Color(1f, 1f, 1f);
        }
    }
}
