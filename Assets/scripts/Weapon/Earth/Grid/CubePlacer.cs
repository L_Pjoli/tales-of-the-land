﻿using UnityEngine;
using Valve.VR;

public class CubePlacer : MonoBehaviour
{
    public GameObject controllerRaycast;
    public GameObject explosionEffect;
    public GenericProjectile pellet;
    public Material terre;
    public int blockNumber = 6;

    private static int index = 0;
    private Grid grid;
    private Terrain terrain;
    private TreeInstance[] treeInstance;

    private const float TERRAIN_HEIGHT = 26f;
    private const float TERRAIN_MAX_WIDTH = 1000f;

    private void Awake(){
        grid = FindObjectOfType<Grid>();
    }

    private void Update(){
        if (SteamVR_Actions._default.GrabPinch.GetStateUp(controllerRaycast.GetComponent<SteamVR_Behaviour_Pose>().inputSource)) {
            bool flag = false;

            RaycastHit hitInfo = getRaycast(controllerRaycast);
            checkForTrees(hitInfo);

            if (!flag) {
                CreateCubesInColumn(hitInfo.point);
            }
        }
    }

    public RaycastHit getRaycast(GameObject objet){
        RaycastHit raycastHit;
        if (Physics.Raycast(objet.transform.position, objet.transform.forward, out raycastHit)){
            return raycastHit;
        }
        return raycastHit;
    }

    public void CreateCubesInColumn(Vector3 clickPoint){
        Vector3 finalPosition = grid.GetNearestPointOnGrid(clickPoint);

        GameObject column = new GameObject();
        column.name = "Colonne" + index;
        index++;

        if (Vector3.Distance(finalPosition, clickPoint) < 1f){
            for (int i = 0; i < blockNumber; i++){
                createCube(i, finalPosition, column);
            }
        }

    }

    private bool checkForTrees(RaycastHit hitInfo){
        terrain = hitInfo.collider.gameObject.GetComponent<Terrain>();
        treeInstance = terrain.terrainData.treeInstances;

        for (int i = 0; i < treeInstance.Length; i++){
            Vector3 currentTreeWorldPosition = Vector3.Scale(new Vector3(treeInstance[i].position.x, TERRAIN_HEIGHT, treeInstance[i].position.z), 
                                                             new Vector3(TERRAIN_MAX_WIDTH, 0, TERRAIN_MAX_WIDTH)) + new Vector3(terrain.transform.position.x, TERRAIN_HEIGHT, terrain.transform.position.z);
            float distance = Vector3.Distance(hitInfo.point, currentTreeWorldPosition);

            if (distance < 1f){
                return true;
            }
        }
        return false;
    }

    private void createCube(int i, Vector3 finalPosition, GameObject column){
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.name = "cube" + i;
        cube.layer = LayerMask.NameToLayer("Cube");
        cube.gameObject.tag = "colonne";

        addComponentToCube(cube, i);

        cube.transform.position = new Vector3(finalPosition.x, finalPosition.y + (grid.size * i), finalPosition.z);
        cube.transform.localScale = new Vector3(grid.size, grid.size, grid.size);
        cube.transform.parent = column.transform;
    }

    void addComponentToCube(GameObject cube, int i){
        cube.AddComponent<SlapCube>();
        cube.GetComponent<SlapCube>().cubeNumber = i;
        cube.GetComponent<SlapCube>().size = grid.size;
        cube.GetComponent<SlapCube>().controllerGO = controllerRaycast;

        cube.AddComponent<ShotGun>();
        cube.GetComponent<ShotGun>().projectile = pellet;

        cube.GetComponent<BoxCollider>().isTrigger = true;
        cube.GetComponent<BoxCollider>().size = new Vector3(1.1f, 1f, 1.1f);

        cube.AddComponent<BoxCollider>().isTrigger = false;

        cube.AddComponent<Rigidbody>().useGravity = false;
        cube.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;

        cube.GetComponent<MeshRenderer>().material = terre;
    }
}