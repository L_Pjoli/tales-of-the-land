﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class SlapCube : MonoBehaviour
{
    public int cubeNumber;
    public float size;
    public GameObject controllerGO;
    public SteamVR_Input_Sources left = SteamVR_Input_Sources.LeftHand;
    public SteamVR_Input_Sources right = SteamVR_Input_Sources.RightHand;

    private const float MIN_VELOCITY = 3f;
    private GameObject column;
    private SteamVR_Behaviour_Pose poseRight;
    private SteamVR_Behaviour_Pose poseLeft;

    private void Start(){
        column = transform.parent.gameObject;

        poseRight = new SteamVR_Behaviour_Pose();
        poseRight.inputSource = right;
        poseLeft = new SteamVR_Behaviour_Pose();
        poseLeft.inputSource = left;

        gameObject.GetComponent<ShotGun>().pelletCount = 10;
        gameObject.GetComponent<ShotGun>().spreadAngle = 5;
    }


    private void OnCollisionEnter(Collision collision) {
        GameObject shootPos = collision.gameObject;

        if (collision.gameObject.tag == "Controller"){
            if (collision.transform.parent.GetComponent<SteamVR_Behaviour_Pose>().inputSource == right){
                shootOnVelocity(poseRight);
            }else if(collision.transform.parent.GetComponent<SteamVR_Behaviour_Pose>().inputSource == left){
                shootOnVelocity(poseLeft);
            }
        }
    }

    private void shootOnVelocity(SteamVR_Behaviour_Pose pose){
        if (pose.GetVelocity().magnitude > 2){
            gameObject.GetComponent<ShotGun>().hand = controllerGO;
            gameObject.GetComponent<ShotGun>().Shoot();
            Destroy(column.transform.GetChild(column.transform.childCount - 1).gameObject);
        }
    }
}
