﻿using UnityEngine;

public class Grid : MonoBehaviour
{

    public float size = 0.5f;
    public int pointsPerRow = 40;
    public GameObject positionPlayerGO;

    private int xCoord = 0;
    private int yCoord = 0;
    private int zCoord = 0;

    public Vector3 GetNearestPointOnGrid(Vector3 position){

        Vector3 positionPlayer = positionPlayerGO.transform.position;

        xCoord = (int) (positionPlayer.x + ((pointsPerRow / 2) * size));
        zCoord = (int) (positionPlayer.z + ((pointsPerRow / 2) * size));

        position -= transform.position;

        int xCount = Mathf.RoundToInt(position.x / size);
        int zCount = Mathf.RoundToInt(position.z / size);

        Vector3 result = new Vector3(
            (float)xCount * size,
            (float)yCoord,
            (float)zCount * size);

        result.x += transform.position.x;
        result.z += transform.position.z;

        return result;
    }
}