﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotGun : GenericWeapon{
    public int pelletCount = 10;
    public float spreadAngle = 5;
    public GameObject hand;

    Vector3 contact;
    public override void Shoot(){
        for (int i = 0; i < pelletCount; i++)
        {
            GameObject pellet = Instantiate(projectile.gameObject, gameObject.transform.position, hand.transform.rotation);
            Debug.Log(pellet.transform.eulerAngles);
            pellet.layer = LayerMask.NameToLayer("pellet");
            pellet.transform.rotation = Quaternion.RotateTowards(pellet.transform.rotation, Random.rotation, spreadAngle);
            pellet.GetComponent<Rigidbody>().velocity = pellet.transform.forward * projectile.projectileVelocity;
            pellet.GetComponent<BoxCollider>().enabled = true;
        }
    }
}
