﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickShotgun : MonoBehaviour {

    public GameObject grid;
    public GenericProjectile projectile;

    public void Start(){
        Shotgun();
    }

    public void Shotgun() {
        grid = Instantiate(grid);
        grid.gameObject.transform.GetChild(0).GetComponent<Grid>().positionPlayerGO = GameObject.Find("Player");
        grid.gameObject.transform.GetChild(1).GetComponent<CubePlacer>().controllerRaycast = gameObject.transform.parent.gameObject;
    }
}
