﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericProjectile : MonoBehaviour{
    public float timeout = 10;
    public float projectileVelocity;
    public float damage;

    private void Start(){
        Destroy(gameObject, timeout);
    }

    public void OnCollisionEnter(Collision collision){
        if (collision.gameObject.GetComponent<Health>() != null){
            collision.gameObject.GetComponent<Health>().DoDamage(damage);
        }
        Effect(collision);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Health>() != null)
        {
            other.gameObject.GetComponent<Health>().DoDamage(damage);
        }
        Debug.Log(other.gameObject.name);
        Destroy(gameObject);
    }

    public void Effect(Collision collision)
    {
        Debug.Log(collision.gameObject.name);
        Destroy(gameObject);
    }
}
