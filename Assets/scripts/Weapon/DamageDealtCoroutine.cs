﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageDealtCoroutine : MonoBehaviour {

    private Color oldColor;
    public Color newColor = Color.red;

    private IEnumerator Start()
    {
        Renderer renderer = gameObject.GetComponent<Renderer>();
        oldColor = renderer.material.color;
        renderer.material.color = newColor;
        yield return new WaitForSecondsRealtime(0.2f);
        renderer.material.color = oldColor;
        Destroy(this);
    }

    public static bool ApplyOn(GameObject target)
    {
        bool apply = target.GetComponent<DamageDealtCoroutine>() == null;
        if (apply)
        {
            DamageDealtCoroutine routine = target.AddComponent<DamageDealtCoroutine>();
            routine.StartCoroutine(routine.Start());
        }
        return apply;
    }
}
