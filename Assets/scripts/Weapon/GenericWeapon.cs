﻿using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class GenericWeapon : MonoBehaviour{
    public GenericProjectile projectile;
    public float coolDownTimeInSeconds;
    protected SteamVR_Input_Sources inputSource;

    private float timeStampCoolDown;


    protected void Start()
    {
        timeStampCoolDown = 0;
        inputSource = gameObject.transform.parent.gameObject.GetComponent<Hand>().handType;
    }

    void Update(){
        if ((Input.GetAxis("Fire1") > 0) || (SteamVR_Actions.default_GrabPinch.GetState(inputSource))) {
            if (timeStampCoolDown <= Time.time) {
                gameObject.GetComponent<GenericWeapon>().Shoot(); 
                timeStampCoolDown = Time.time + coolDownTimeInSeconds;
            }
        }
    }

    public virtual void Shoot()
    {
        GameObject tempProjectile = Instantiate(projectile.gameObject);
        tempProjectile.transform.position = gameObject.transform.position;
        Debug.Log(gameObject.name);
        tempProjectile.GetComponent<Rigidbody>().velocity = gameObject.transform.forward * projectile.projectileVelocity;
    }
}
